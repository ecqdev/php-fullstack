import { Component, OnInit } from '@angular/core';
import { VeiculoService } from './services/veiculo.service';
import { Veiculo } from './models/veiculo';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  veiculo = {} as Veiculo;
  veiculos: Veiculo[];

  constructor(private carService: VeiculoService) {}
  
  ngOnInit() {
    let veiculos = this.getCars();    
    
  }

  // defini se um carro será criado ou atualizado
  saveCar(form: NgForm) {
    if (this.veiculo.id !== undefined) {
      this.carService.updateCar(this.veiculo).subscribe(() => {
        this.cleanForm(form);
      });
    } else {
      this.carService.saveCar(this.veiculo).subscribe(() => {
        this.cleanForm(form);
      });
    }
  }

  // Chama o serviço para obtém todos os carros
  getCars() {
    this.carService.getCars().subscribe((cars: Veiculo[]) => {
      this.veiculos = cars;
    });
  }

  // deleta um carro
  deleteCar(car: Veiculo) {
    this.carService.deleteCar(car).subscribe(() => {
      this.getCars();
    });
  }

  // copia o carro para ser editado.
  editCar(car: Veiculo) {
    this.veiculo = { ...car };
  }

  // limpa o formulario
  cleanForm(form: NgForm) {
    this.getCars();
    form.resetForm();
    this.veiculo = {} as Veiculo;
  }

  //Search
  searchCar(){
   
  }
}
