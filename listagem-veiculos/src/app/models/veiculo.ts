export interface Veiculo {
    id:		    number;
    veiculo:   	string;
    marca:     	string;
    ano:       	number;
    descricao: 	string;
    vendido:   	boolean;
    str_vendido: string;
    created:   	Date;
    updated:   	Date;
    
}

