<?php


namespace App\Http\Controllers;


use App\Veiculo;
use Illuminate\Http\Request;

class VeiculosController
{
    public function index()
    {
        return Veiculo::all();
    }

    public function store(Request $request)
    {
        return response()
            -> json (
                Veiculo::create($request->all()),
                201
                );
    }

    public function getSearchResults(string $parametro, Request $request)
    {
        $search = $parametro; //$request->get('search');
        $veiculo = Veiculo::where('veiculo', 'LIKE', '%' . $search . '%')
            ->orWhere('marca', 'LIKE', '%' . $search . '%')
            ->orWhere('ano', 'LIKE', '%' . $search . '%')
            ->orWhere('vendido', 'LIKE', '%' . $search . '%')
            ->orWhere('descricao', 'LIKE', '%' . $search . '%')
            ->paginate(10);


        if (is_null($veiculo)){
            return response() -> json('', 204);
        }
        return response() -> json($veiculo);
    }

    public function show(int $id)
    {
        $veiculo = Veiculo::find($id);
        if (is_null($veiculo)){
            return response() -> json('', 204);
        }
        return response() -> json($veiculo);
    }

    public function update(int $id, Request $request)
    {
        $veiculo = Veiculo::find($id);
        if (is_null($veiculo)){
            return response()->json ([
                'erro' => 'Veículo não encontrado'
                ],404);
        }
        $veiculo->fill($request->all());
        $veiculo->save();
        return response() -> json ($veiculo);
    }

    public function destroy(int $id)
    {
        $qtdRecursosRemovidos = Veiculo::destroy($id);
        if($qtdRecursosRemovidos === 0) {
            return response() -> json ([
               'erro' => 'Veículo não encontrado'
            ], 404);
        }
        return response() -> json('', 204);
    }
}
