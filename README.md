# PHP FullStack

::: API :::

PHP 7.4
Laravel/Lumen - *** Necessário criar projeto para gerar pasta vendor

http://localhost:8090/api/veiculos

MYSQL 8 
=> Criar database veiculos => CREATE DATABASE veiculos

Executar API:

=> php artisan make:migration criar_tabela_veiculos --create=veiculos
=> php artisan migrate
=> php -S localhost:8090 -t public



::: FrontEnd :::

Angular 8

Executar Front:
npm start --open


::: Browser :::

Desabilitar Web Security para utilizar CORS (GET/PUT/DELETE/OPTIONS)
Acessar pasta do Chrome
=> chrome.exe --user-data-dir="C:Chrome dev session" --disable-web-security


